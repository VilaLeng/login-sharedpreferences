import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Login Page",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {
  /*late SharedPreferences prefs;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
   String Email = "";*/
  /*@override
  void initState() {
    super.initState();
    _loadCounter();
  }

  //Loading counter value on start
  void _loadCounter() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      emailController = (prefs.getString(Email) ?? 'Email') as TextEditingController;
    });
  }*/
  /*@override
  void initState() {
    _loadVals();
    super.initState();
  }
  _loadVals() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('Email', 'My string value');
    Email = prefs.getString('aStringValue')!;
    print("After fetch '$Email'");
  }*/
  late SharedPreferences prefs;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String Email = "";
  @override
  void initState() {
    super.initState();
    _dataLogin();
  }

  void _dataLogin() async {
    prefs = await SharedPreferences.getInstance();
    Email = prefs.getString("Email") ?? '';
    setState(() {
      emailController.text = Email;
    });
  }




/*@override
  void initState() {
    super.initState();
    readData();
  }
  void readData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('Email') == null)
      setState(() => Email = '0');
    else
      setState(() => Email = prefs.getString('Email')!);
  }*/




/*
late bool textLoaded;

 get sharedPreferences => null;
  @override
  void initState() {
    textLoaded = false;
    super.initState();
    setText();
  }

  Future<void> setText() async {
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs = await SharedPreferences.getInstance();
    //Email = sharedPreferences.getString("Email");
    */
/*SharedPreferences prefs = await SharedPreferences.getInstance(); // create shared preference obj
   // prefs = await SharedPreferences.getInstance();
    setState(() async {
      prefs = await SharedPreferences.getInstance();
      //SharedPreferences prefs = await SharedPreferences.getInstance();
      Email = "GET TEXT FROM PREF";
    emailController = sharedPreferences.getString(Email);
    textLoaded = true;
    });// get data from shared preference
    setState(() async {
      prefs = await SharedPreferences.getInstance();
       Email = "GET TEXT FROM PREF";
      //emailController = TextEditingController(text: Email);
      emailController = sharedPreferences.getString(Email);
      textLoaded=true;

    });*//*

    setState(() async {
     prefs = await SharedPreferences.getInstance();
     //SharedPreferences prefs = await SharedPreferences.getInstance();
      //Email = "GET TEXT FROM PREF";
     Email = sharedPreferences.getString("Email");
      //emailController = TextEditingController(text: 'Email168');
      emailController = TextEditingController(text: Email);
     // emailController = TextEditingController(text: sharedPreferences.getString("Email"));
       emailController = sharedPreferences.getString("Email");
      textLoaded = true;
    });
*/


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Page"),
      ),
      body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
/*
              textLoaded
                  ? TextField(
                controller: emailController,
              )
                  : const CircularProgressIndicator(),*/
              /*RaisedButton(
                onPressed: () {
                  emailController.clear();
                },
                child: const Text('CLEAR'),
              ),*/
              Container(
                padding: const EdgeInsets.all(10),
                child: Image.asset('assets/Google-flutter-logo.png'),
              ),
              /*if (textLoaded) TextField(
                controller: emailController,
               // controller: sharedPreferences.toString(setText()),
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  // labelText: 'Email',
                ),
              ) else Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                  ),
                ),
              ),*/
              
              Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  controller:emailController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',

                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: TextField(
                  obscureText: true,
                  controller: passwordController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                  ),
                ),

              ),
              SizedBox(
                height: 30,
              ),
              Container(
                  height: 50,
                  width: 200,
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(
                    child: const Text('Login',style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),),
                    onPressed: () {
                      save();
                    },
                  )
              ),

              SizedBox(
                height: 30,
              ),
              /*Text(Email, style: TextStyle(fontSize: 20),),
              SizedBox(
                height:30,
              ),
*/
            ],
          )),
    );
  }

  save() async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString("Email", emailController.text.toString());
    prefs.setDouble("quan", 100.50);
    prefs.setBool("boolvalue", true);
  }



/*@override
  void initState(){
     getNamePreference().then(updateName);
     super.initState();
  }
  void updateName(String Email){
     setState(() {
       this.Email = Email;
     });
  }
*/


}
